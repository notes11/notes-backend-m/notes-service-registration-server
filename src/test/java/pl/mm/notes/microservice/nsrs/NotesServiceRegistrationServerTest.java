package pl.mm.notes.microservice.nsrs;

import io.restassured.RestAssured;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT, classes = NotesServiceRegistrationServer.class)
public class NotesServiceRegistrationServerTest extends AbstractTestNGSpringContextTests {

    @SuppressWarnings("FieldMayBeFinal")
    @Value("${server.port}")
    private int serverPort = -1;

    @BeforeClass
    public void beforeClassSetUp() {
        RestAssured.port = serverPort;
    }

    @Test
    public void testNotesServiceRegistrationServer() {
        when()
                .get()
                .then()
                .statusCode(200);
    }

}