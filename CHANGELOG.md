# notes-service-registration-server

## 0.0.1 (2021-05-17)
Version reset to 0.0.1

## 0.0.4 (2021-05-16)
Correction in CI stage docker-image-release - v3

## 0.0.3 (2021-05-16)
Correction in CI stage docker-image-release - v2

## 0.0.2 (2021-05-16)
Correction in CI stage docker-image-release

## 0.0.1 (2021-05-16)
Initial release