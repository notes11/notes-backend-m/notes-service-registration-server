FROM openjdk:14-jdk-alpine
COPY target/notes-service-registration-server-*.jar /notes-service-registration-server.jar
ENTRYPOINT ["java", "-jar", "/notes-service-registration-server.jar"]